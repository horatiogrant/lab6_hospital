/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Bank;

import java.util.ArrayList;

/**
 *
 * @author grant
 */
public class Bank {
    private String name;
    private ArrayList <Employee> emp ;

    public Bank(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Employee> getEmp() {
        return emp;
    }

    public void setEmp(ArrayList<Employee> emp) {
        this.emp = emp;
    }
    
    public void addEmp(Employee emp){
        this.emp.add(emp);
    }
    public void removeEmp(Employee emp){
        this.emp.remove(emp);
    }  
}
